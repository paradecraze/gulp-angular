# Gulp Angular
Gulp Angular provides the development environment and build pipeline for AngularJS applications.

## Installation

    $ npm install -g gulp

Add the following to devDependencies in package.json:

    "gulp-angular": "git+https://bitbucket.org/paradecraze/gulp-angular.git#0.2.4"

## GIT Tags

Add tag in GIT

    git tag -a 0.0.X -m "added tag 0.0.X"

Push repo with tags

    git push origin master --tags

## Usage

    var gulp = require('gulp');
    var gulpAngular = require('gulp-angular');

    var options = {};

    gulpAngular(gulp, options);

## Options

**options.javascriptsTest** (Array) - List of vendor files to be provided to karma configuration. The order of the files will be honored. __Note:__ 'app/**/*.js' is automatically appended to this list.

**options.javascriptsVendorDev** (Array) - List of vendor javascripts to be added to index.html in development environment. Add `<!-- vendor:js --><!-- endinject -->` to app/index.html to inject these files.

**options.stylesheetsVendorDev** (Array) - List of vendor stylesheets (.css) to be added to index.html in development environment. Add `<!-- vendor:css --><!-- endinject -->` to app/index.html to inject these files.

**options.javascriptsVendorProduction** (Array) - List of vendor javascripts to be added to index.html in production build.  Defaults to values provided in javascriptsVendorDev. Add `<!-- vendor:js --><!-- endinject -->` to app/index.html to inject these files.

**options.stylesheetsVendorProduction** (Array) - List of vendor stylesheets (.css) to be added to index.html in production build. Defaults to values provided in stylesheetsVendorDev. Add `<!-- vendor:css --><!-- endinject -->` to app/index.html to inject these files.

## index.html
Example:

    <!DOCTYPE html>
    <html ng-app="app">
    <head lang="en">
      <meta charset="UTF-8">
      <title>App Name</title>
      <link rel="icon" href="favicon.ico">

      <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans">
      <!-- fonts:css -->
      <!-- endinject -->
      <!-- vendor:css -->
      <!-- endinject -->
      <!-- source:css -->
      <!-- endinject -->

      <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
      <!-- vendor:js -->
      <!-- endinject -->
      <!-- source:js -->
      <!-- endinject -->
    </head>
    <body>
      <main ui-view="container"></main>
    </body>
    </html>

## App Directory Structure
Use the following directory structure for use with gulp-angular.

    .
    +-- app/
    |   +-- assets/
    |   |   +-- fonts/
    |   |   +-- icons/
    |   |   +-- images/
    |   +-- directives/
    |   |   +-- pzHeader.js
    |   +-- stylesheets/
    |   |   +-- layout.scss
    |   |   +-- reset.scss
    |   +-- templates/
    |   |   +-- index.html
    |   |   +-- pzHeader.html
    |   +-- website/
    |   |   +-- templates/
    |   |   |   +-- index.html
    |   |   +-- stylesheets/
    |   |   |   +-- website.scss
    |   |   +-- website.module.js
    |   |   +-- websiteController.js
    |   |   +-- websiteControllerTest.js
    |   +-- app.js
    |   +-- index.html
    +-- config/
    |   +-- environments/
    |   |   +-- development.json
    +-- test/
    +-- vendor/
    +-- bower.json
    +-- gulpfile.js
    +-- index.js
    +-- package.json

* All source code and assets for the client SPA should be placed under the app folder.
* It is recommended that your source files be organized in modules that live under their own sub-directory in the app folder.
* Files of the format *Test.js will not be added to index.html and they will be run as Unit Tests.
* The SVG files in the icons folder will be processed with inline-svg and a .scss will be generated so these svgs can be used in stylesheets.
* Fonts in app/assets/fonts will be copied to build/ and dist/ directories verbatim.
* Images in app/assets/images will be optimized for web access.
* The values in config/environments/*.json will be turned into angular constants and injected into a module that can be consumed by your application.
* Third-party dependencies that are not deliverable via bower should be placed in the vendor folder.

## Deployment

### Continuous Integration
Each time the master branch is commited the following steps will run in CI.

#### Environment Variables

    AWS_ACCESS_KEY_ID='ABC123'
    AWS_SECRET_ACCESS_KEY='DEF456'
    AWS_DEV_BUCKET='dev.paradecraze'
    AWS_REGION='us-west-2'
    AWS_UPLOAD_PATH=''
    AWS_NAMESPACE='pz-app'

#### Build, Test, and Deployment Steps

    $ nvm install 4.2.1
    $ npm set progress=false
    $ npm install -g bower
    $ npm install

    $ gulp unitTests

    $ gulp build-production
    $ gulp deploy

### Promotion

    $ gulp promote-to-staging
    $ gulp promote-to-production

### Integrating into Rails App

* **gulp deploy** will push the compiled app into S3/bucket/namespace  (s3.com/dev.paradecraze/pz-store)
* The rails app will read dev.paradecraze/pz-store/index.html and deliver it at http://dev.store.paradecraze.com
* TODO: fingerprint the entire build.  Have a redis instance pull the HTML for that fingerprint and deliver it.

#### Questions

* How do we get the contents of index.html into redis automatically for every deployment?
* Do we have a different redis instance for each environment?
