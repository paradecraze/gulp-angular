function getNamespace(namespace) {
  return (namespace !== '') ? namespace + '/' : '';
}

module.exports = exports = function(gulp, options) {
  var _ = require('lodash');
  var gulpAngularFileSort = require('gulp-angular-filesort');
  var gulpAngularTemplateCache = require('gulp-angular-templatecache');
  var gulpAutoPrefixer = require('gulp-autoprefixer');
  var gulpConcat = require('gulp-concat');
  var gulpCssNano = require('gulp-cssnano');
  var gulpDel = require('del');
  var gulpFlatten = require('gulp-flatten');
  var gulpIconFont = require('gulp-iconfont');
  var gulpIconFontCss = require('gulp-iconfont-css');
  var gulpImageMin = require('gulp-imagemin');
  var gulpInject = require('gulp-inject');
  var gulpJsHint = require('gulp-jshint');
  var gulpNgConstant = require('gulp-ng-constant');
  var gulpRename = require('gulp-rename');
  var gulpRevAll = require('gulp-rev-all');
  var gulpRunSequence = require('run-sequence').use(gulp);
  var gulpSass = require('gulp-sass');
  var gulpUglify = require('gulp-uglify');
  var gulpUtil = require('gulp-util');
  var browserSync = require('browser-sync').create();
  var karmaConfig = require('./karma.conf.js');
  var karmaServer = require('karma').Server;
  var path = require('path');
  var protractor = require('gulp-protractor').protractor;
  var q = require('q');
  var s3 = require("gulp-s3");
  var spawn = require('child_process').spawn;
  var ts = require('gulp-typescript');
  var mergeStream = require('merge-stream');

  var namespace = getNamespace(options.namespace);
  var buildFolder = 'build/' + namespace;
  var productionFolder = 'dist/' + namespace;
  var config = {
    aws: options.aws,
    awsOptions: options.awsOptions,
    browserSync: {
      port: options.browserSyncPort || 3001,
    },
    dir: {
      development: buildFolder,
      devAssets: buildFolder + 'assets/',
      devFonts: buildFolder + 'assets/fonts/',
      devIconFont: buildFolder + 'assets/fonts/icon-font/',
      devJavascripts: buildFolder + 'javascripts/',
      devStylesheets: buildFolder + 'stylesheets/',
      devVendorJavascripts: buildFolder + 'vendor/javascripts/',
      devVendorStylesheets: buildFolder + 'vendor/stylesheets/',
      devTest: buildFolder + 'test/',
      devTestE2E: buildFolder + 'e2e_test/',
      environments: 'config/environments/',
      production: productionFolder,
      productionAssets: productionFolder + 'assets/',
      productionFonts: productionFolder + 'assets/fonts/',
      productionIconFont: productionFolder + 'assets/fonts/icon-font/',
      productionImages: productionFolder + 'assets/images/',
      temp: 'tmp/'
    },
    files: {
      assets: ['app/assets/**/*', '!app/assets/icons', '!app/assets/icons/**/*'],
      developmentBuildOutput: buildFolder + '**/*',
      productionBuildOutput: productionFolder + '**/*',
      environments: 'config/environments/*.json',
      expressServer: 'index.js',
      fonts: 'app/assets/fonts/**',
      gulpfile: 'gulpfile.js',
      icons: 'app/assets/icons/**/*.svg',
      iconFontScss: 'temp/icon-font/icon-font.scss',
      index: 'app/index.html',
      indexProduction: productionFolder + 'index.html',
      images: 'app/assets/images/*',
      javascriptSrcProduction: productionFolder + 'app.min.js',
      javascriptVendorProduction: productionFolder + 'vendor.min.js',
      javascriptsGenerated: ['tmp/**/*.js'],
      javascriptsSrc: ['app/**/*.js', 'app/**/*.ts', '!app/**/*Test.js', '!app/**/*Test.ts', '!app/**/*Spec.ts', '!app/**/*Spec.js'],
      javascriptsTests: ['app/**/*Test.js', 'app/**/*Test.ts'],
      javascriptsTestsE2E: ['app/**/*Spec.js', 'app/**/*Spec.ts'],
      karmaConfiguration: 'test/karma.conf.js',
      revisionManifest: productionFolder + 'rev-manifest.json',
      stylesheetsSrcDev: 'app/**/*.scss',
      stylesheetSrcProduction: productionFolder + 'site.min.css',
      stylesheetVendorProduction: productionFolder + 'vendor.min.css',
      templates: ['app/**/*.html', '!app/index.html'],
      typings: [
        'typings/**/*.d.ts',
        '!typings/browser.d.ts',
        '!typings/browser/**/*'
      ]
    }
  };

  config.files.javascriptsVendorDev = options.javascriptsVendorDev || [];
  config.files.javascriptsVendorProduction = options.javascriptsVendorProduction || config.files.javascriptsVendorDev;
  config.files.javascriptsTest = options.javascriptsTest || [];
  config.files.stylesheetsVendorDev = options.stylesheetsVendorDev || [];
  config.files.stylesheetsVendorProduction = options.stylesheetsVendorProduction || config.files.stylesheetsVendorDev;

  function notImplemented(feature) {
    throw new gulpUtil.PluginError('gulp-angular', {
      message: feature + ' is not yet implemented.'
    });
  }

  function compileStylesheets(source, outFile) {
    return gulp.src(source)
      // Compile SCSS to CSS
      .pipe(gulpSass().on('error', gulpSass.logError))
      // Add browser prefixes
      .pipe(gulpAutoPrefixer('last 2 versions'))
      // Concatenate all css files
      .pipe(gulpConcat(outFile))
      // Compress stylesheet
      .pipe(gulpCssNano());
  }

  function fontPath(env) {
    if (env === 'production') {
      return 'assets/fonts/icon-font/';
    }

    return '/assets/fonts/icon-font/';
  }

  function iconFont(env) {
    var FONT_NAME = 'icon-font';
    var FONT_PATH = fontPath(env);

    return gulp.src(config.files.icons)
      .pipe(gulpIconFontCss({
        className: 'icon',
        fontName: FONT_NAME,
        fontPath: FONT_PATH,
        targetPath: FONT_NAME + '.css'
      }))
      .pipe(gulpIconFont({
        appendUnicode: false,
        fontName: FONT_NAME,
        fontPath: FONT_PATH,
        formats: ['svg', 'ttf', 'eot', 'woff', 'woff2'],
        normalize: true,
        timestamp: Math.round(Date.now() / 1000)
      }));
  }

  function jsHint(source) {
    return gulp.src(source)
      .pipe(gulpJsHint())
      .pipe(gulpJsHint.reporter('default'));
  }

  gulp.task('assets-development', function() {
    return gulp.src(config.files.assets)
      .pipe(gulp.dest(config.dir.devAssets));
  });

  gulp.task('assets-production', function() {
    return gulp.src(config.files.assets)
      .pipe(gulpImageMin({
        optimizationLevel: 3,
        progressive: true,
        interlaced: true
      }))
      .pipe(gulp.dest(config.dir.productionAssets));
  });

  gulp.task('build-development', function() {
    return gulpRunSequence(
      'environmentDevelopmentConfiguration',
      'templates',
      'icons-development',
      'assets-development',
      'stylesheetsSrc-development',
      'stylesheetsVendor-development',
      'javascriptsSrc-development',
      'javascriptsVendor-development',
      'index-development'
    );
  });

  gulp.task('build-production', function() {
    return gulpRunSequence(
      'environmentProductionConfiguration',
      'templatesProduction',
      'icons-production',
      'assets-production',
      'stylesheetsSrc-production',
      'stylesheetsVendor-production',
      'javascriptsSrc-production',
      'javascriptsVendor-production',
      'index-production',
      'revisionAssets'
    );
  });

  gulp.task('templatesProduction', function() {
    return gulp.src(config.files.templates)
      .pipe(gulpAngularTemplateCache({module: 'app.templates', standalone: true}))
      .pipe(gulp.dest(config.dir.temp));
  });

  gulp.task('clean', function() {
    return gulpDel([config.dir.development, config.dir.production, config.dir.temp]);
  });

  //TODO: refactor the environment configuration for development and prod
  gulp.task('environmentProductionConfiguration', function() {
    return gulp.src(config.dir.environments + 'production.json')
      .pipe(gulpNgConstant({
        name: 'app.config',
        constants: {
          APPLICATION_NAME: options.applicationName,
          COMPANY_NAME: options.companyName
        }
      }))
      .pipe(gulpRename('config.js'))
      .pipe(gulp.dest(config.dir.temp));
  });

  gulp.task('environmentDevelopmentConfiguration', function() {
    return gulp.src(config.dir.environments + 'development.json')
      .pipe(gulpNgConstant({
        name: 'app.config',
        constants: {
          APPLICATION_NAME: options.applicationName,
          COMPANY_NAME: options.companyName
        }
      }))
      .pipe(gulpRename('config.js'))
      .pipe(gulp.dest(config.dir.temp));
  });

  gulp.task('default', ['clean'], function() {
    return gulpRunSequence('build-development', 'serve-development');
  });

  gulp.task('e2e', function() {
    gulp.src(config.files.javascriptsTestsE2E)
      .pipe(ts({
        target: 'ES5',
        typescript: require('typescript'),
        noExternalResolve: false,
        noImplicitAny: false,
        module: 'commonjs',
        allowJs: true
      }))
      .pipe(gulp.dest(config.dir.devTestE2E))
      .pipe(protractor({
        configFile: 'test/protractor.conf.js'
      }));
  });
  gulp.task('icons-development', function() {
    return iconFont('development')
      .pipe(gulp.dest(config.dir.devIconFont));
  });
  gulp.task('icons-production', function() {
    return iconFont('production')
      .pipe(gulp.dest(config.dir.productionIconFont));
  });
  gulp.task('index-development', function() {
    // Because the order of vendor files must be correct, we cannot use a glob
    // pattern from the build output.
    var vendorJs = config.files.javascriptsVendorDev.map(function(file) {
      return config.dir.devVendorJavascripts + path.basename(file);
    });
    var vendorCss = config.files.stylesheetsVendorDev.map(function(file) {
      return config.dir.devVendorStylesheets + path.basename(file);
    });

    var fonts = gulp.src(config.dir.devFonts + '**/*.css');
    var jsSource = gulp.src(config.dir.devJavascripts + '**/*.js').pipe(gulpAngularFileSort());
    var jsVendor = gulp.src(vendorJs);
    var cssSource = gulp.src(config.dir.devStylesheets + '**/*.css');
    var cssVendor = gulp.src(vendorCss);

    var gulpInjectVendorOptions = {
      addRootSlash: false,
      ignorePath: config.dir.development,
      name: 'vendor',
      removeTags: true
    };
    var gulpInjectSourceOptions = {
      addRootSlash: false,
      ignorePath: config.dir.development,
      name: 'source',
      removeTags: true
    };
    var gulpInjectFontsOptions = {addRootSlash: false, ignorePath: config.dir.development, name: 'fonts', removeTags: true};

    return gulp.src(config.files.index)
      .pipe(gulpInject(fonts, gulpInjectFontsOptions))
      .pipe(gulpInject(jsVendor, gulpInjectVendorOptions))
      .pipe(gulpInject(jsSource, gulpInjectSourceOptions))
      .pipe(gulpInject(cssVendor, gulpInjectVendorOptions))
      .pipe(gulpInject(cssSource, gulpInjectSourceOptions))
      .pipe(gulp.dest(config.dir.development));
  });
  gulp.task('index-production', function() {
    var fonts = gulp.src(config.dir.productionFonts + '**/*.css');
    var jsSource = gulp.src(config.files.javascriptSrcProduction);
    var jsVendor = gulp.src(config.files.javascriptVendorProduction);
    var cssSource = gulp.src(config.files.stylesheetSrcProduction);
    var cssVendor = gulp.src(config.files.stylesheetVendorProduction);

    var gulpInjectVendorOptions = {
      addRootSlash: false,
      ignorePath: config.dir.production,
      name: 'vendor',
      removeTags: true
    };
    var gulpInjectSourceOptions = {
      addRootSlash: false,
      ignorePath: config.dir.production,
      name: 'source',
      removeTags: true
    };
    var gulpInjectFontsOptions = {
      addRootSlash: false,
      ignorePath: config.dir.production,
      name: 'fonts',
      removeTags: true
    };

    return gulp.src(config.files.index)
      .pipe(gulpInject(fonts, gulpInjectFontsOptions))
      .pipe(gulpInject(jsVendor, gulpInjectVendorOptions))
      .pipe(gulpInject(jsSource, gulpInjectSourceOptions))
      .pipe(gulpInject(cssVendor, gulpInjectVendorOptions))
      .pipe(gulpInject(cssSource, gulpInjectSourceOptions))
      .pipe(gulp.dest(config.dir.production));
  });

  gulp.task('javascriptsSrc-development', function() {
    var files = config.files.javascriptsSrc.concat(config.files.typings)
      .concat(config.files.javascriptsGenerated);

    return gulp.src(files)
      .pipe(ts({
        target: 'ES5',
        typescript: require('typescript'),
        noExternalResolve: true,
        noImplicitAny: false,
        module: 'commonjs',
        allowJs: true
      }))
      .pipe(gulp.dest(config.dir.devJavascripts));
  });
  gulp.task('javascriptsSrc-production', function() {
    var files = config.files.javascriptsSrc.concat(config.files.typings)
            .concat(config.files.javascriptsGenerated);

    return gulp.src(files)
      .pipe(ts({
        target: 'ES5',
        typescript: require('typescript'),
        noExternalResolve: true,
        noImplicitAny: false,
        module: 'commonjs',
        allowJs: true
      }))
      // Sort source files
      .pipe(gulpAngularFileSort())
      // Concatenate all files into app.min.js
      .pipe(gulpConcat('app.min.js'))
      // Uglify code
      .pipe(gulpUglify())
      // Place in production directory
      .pipe(gulp.dest(config.dir.production));
  });
  gulp.task('javascriptsTest-development', function() {
    return jsHint(config.files.javascriptsTests);
  });
  gulp.task('javascriptsVendor-development', function() {
    return gulp.src(config.files.javascriptsVendorDev)
      // Place in development vendor directory
      .pipe(gulp.dest(config.dir.devVendorJavascripts));
  });
  gulp.task('javascriptsVendor-production', function() {
    return gulp.src(config.files.javascriptsVendorProduction)
      // Concatenate all files into vendor.min.js
      .pipe(gulpConcat('vendor.min.js'))
      // Uglify code
      .pipe(gulpUglify())
      // Place in production directory
      .pipe(gulp.dest(config.dir.production));
  });
  gulp.task('jshint-gulpfile', function() {
    return gulp.src(config.files.gulpfile)
      .pipe(gulpJsHint())
      .pipe(gulpJsHint.reporter('default'));
  });
  gulp.task('revisionAssets', function() {
    var deferred = q.defer();

    var revAll = new gulpRevAll({
      dontRenameFile: [ 'index.html' ],
      transformPath: function(rev, source, path) {
        return '/' + namespace + rev.replace(/^\//, '');
      }
    });

    gulp.src([ config.dir.production + '**' ])
      .pipe(revAll.revision())
      //.pipe(gulpRevCssUrl())
      .pipe(gulp.dest(config.dir.production))
      .pipe(revAll.manifestFile(config.files.revisionManifest))
      .pipe(gulp.dest(config.dir.production))
      .on('end', function() {
        // Remove original files
        var revisionManifestPath = path.resolve('./', config.files.revisionManifest);
        var revisionManifest = require(revisionManifestPath);
        var originalFiles = Object.keys(revisionManifest);

        gulpDel(_.without(originalFiles, 'index.html'), { cwd: config.dir.production })
          .then(function() { deferred.resolve(); });
      });

    return deferred.promise;
  });
  gulp.task('serve-development', function() {
    return gulpRunSequence('watch-development');
  });
  gulp.task('serve-production', function() {
    spawn('node', [config.files.expressServer, config.dir.production]);
    return gulpRunSequence('watch-production');
  });
  gulp.task('stylesheetsSrc-development', function() {
    return gulp.src(config.files.stylesheetsSrcDev)
      // Compile SCSS to CSS
      .pipe(gulpSass({includePaths: []}).on('error', gulpSass.logError))
      // Automatically add browser prefixes
      .pipe(gulpAutoPrefixer('last 2 versions'))
      // Flatten directory structure for easier include
      .pipe(gulpFlatten())
      // Place in development directory
      .pipe(gulp.dest(config.dir.devStylesheets));
  });
  gulp.task('stylesheetsSrc-production', function() {
    return compileStylesheets(config.files.stylesheetsSrcDev, 'site.min.css')
      // Place in dist directory
      .pipe(gulp.dest(config.dir.production));
  });
  gulp.task('stylesheetsVendor-development', function() {
    return gulp.src(config.files.stylesheetsVendorDev)
      // Place in development vendor directory
      .pipe(gulp.dest(config.dir.devVendorStylesheets));
  });
  gulp.task('stylesheetsVendor-production', function() {
    return compileStylesheets(config.files.stylesheetsVendorProduction, 'vendor.min.css')
      // Place in dist directory
      .pipe(gulp.dest(config.dir.production));
  });
  gulp.task('templates', function() {
    return gulp.src(config.files.templates)
      .pipe(gulpAngularTemplateCache({module: 'app.templates', standalone: true}))
      .pipe(gulp.dest(config.dir.devJavascripts));
  });
  gulp.task('unitTests', function(callback) {
    var sourceFiles = [];

    var appFiles = config.files.javascriptsSrc;
    var testFiles = config.files.javascriptsTests;
    var testDeps = gulp.src(config.files.javascriptsTest);

    // Compile angular source files
    var appFileStream = gulp.src(appFiles)
      .pipe(ts({
        target: 'ES5',
        typescript: require('typescript'),
        noExternalResolve: false,
        noImplicitAny: false,
        module: 'commonjs',
        allowJs: true
      }))
      .pipe(gulpAngularFileSort());

    // Must compile test files separately because config.files.javascriptsSrc
    // excludes test files.
    var testFileStream = gulp.src(testFiles)
      .pipe(ts({
        target: 'ES5',
        typescript: require('typescript'),
        noExternalResolve: false,
        noImplicitAny: false,
        module: 'commonjs',
        allowJs: true
      }));

    // Sort angular files for proper load order.
    mergedAppFiles = mergeStream(appFileStream, testFileStream)
      .pipe(gulpAngularFileSort());

    merged = mergeStream(testDeps, mergedAppFiles)
      .pipe(gulp.dest(config.dir.devTest));

    merged.on('data', function(file) {
      sourceFiles.push(file.path);
    });

    merged.on('end', function() {
      karmaConfig.files = sourceFiles;

      // Start karma server with configuration loaded from karma.conf.js
      new karmaServer(karmaConfig, callback).start();
    });
  });

  gulp.task('deploy', function() {
    gulp.src('./dist/**')
      .pipe(s3(config.aws, config.awsOptions));
  });

  gulp.task('promote-to-staging', function() {
    notImplemented('promote-to-staging');
  });

  gulp.task('promote-to-production', function() {
    notImplemented('promote-to-production');
  });

  gulp.task('watch-development', function() {
    browserSync.init({
      port: config.browserSync.port,
      server: {
        baseDir: config.dir.development
      }
    });

    gulp.watch(config.files.assets, ['assets-development']);
    gulp.watch(config.files.environments, ['environmentConfiguration']);
    gulp.watch(config.files.icons, ['icons-development']);
    gulp.watch(config.files.index, ['index-development']);
    gulp.watch(config.files.javascriptsSrc, function() {
      return gulpRunSequence('javascriptsSrc-development', 'index-development', 'unitTests');
    });
    gulp.watch(config.files.javascriptsTests, function() {
      return gulpRunSequence('javascriptsTest-development', 'unitTests');
    });
    gulp.watch(config.files.stylesheetsSrcDev, function() {
      return gulpRunSequence('stylesheetsSrc-development', 'index-development');
    });
    gulp.watch(config.files.templates, ['templates']);

    gulp.watch(config.files.developmentBuildOutput).on('change', browserSync.reload);
  });

  gulp.task('watch-production', function() {
    var toWatch = [
      config.files.assets,
      config.files.environments,
      config.files.icons,
      config.files.index,
      config.files.javascriptsSrc
    ];

    gulp.watch(toWatch, function() {
      return gulpRunSequence('clean', 'build-production');
    });
  });

  gulp.task('watch', function() {
    return gulpRunSequence('watch-development');
  });
};
